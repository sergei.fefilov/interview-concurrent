package local.interviews.linkedlists;

public class LinkedListNode<T> {
        public T value;
        public LinkedListNode<T> next;

        public LinkedListNode(T value, LinkedListNode<T> next) {
            this.value = value;
            this.next = next;
        }
}
