package local.interviews.linkedlists;

/*
 найди цикл в связном списке, если цикл есть вернуть элемент, где цикл начинается, иначе null
 */
public interface FindCycleStart {
    LinkedListNode<?> find(LinkedListNode<?> first);
}
