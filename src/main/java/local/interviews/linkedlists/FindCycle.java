package local.interviews.linkedlists;

/*
 найди цикл в связном списке, если цикл есть вернуть true, иначе false
 */
public interface FindCycle {
    boolean find(LinkedListNode<?> first);
}
