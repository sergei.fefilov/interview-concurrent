package local.interviews.concurrency;

import java.util.UUID;
public interface TaskGenerator {
    UUID generate(UUID taskId, Boolean fast);
}
