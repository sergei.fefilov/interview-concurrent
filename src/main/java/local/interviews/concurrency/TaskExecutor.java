package local.interviews.concurrency;

import java.util.UUID;

public interface TaskExecutor {
    UUID execute(UUID taskId, Boolean fast);
}
