package local.interviews;

import com.google.common.reflect.ClassPath;
import lombok.SneakyThrows;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;

import java.lang.reflect.ParameterizedType;
import java.util.*;
import java.util.stream.Collectors;

public class ImplementationsScan implements ParameterResolver {
    private final Object lock = true;
    private static volatile Set<Class<?>> loadedClasses;

    @Override
    public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext)
            throws ParameterResolutionException {
        return getInterface(parameterContext) != null;
    }

    @Override
    public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext)
            throws ParameterResolutionException {

        var impl = scanImplementationsAndCreate(getInterface(parameterContext));

        if (!parameterContext.getParameter().getType().isAssignableFrom(List.class)) {
            if (impl.size() > 1) {
                throw new IllegalStateException("several implementations found for "
                        + parameterContext.getParameter().getType().getSimpleName());
            }
            return impl.get(0);
        } else {
            return impl;
        }
    }

    private Class<?> getInterface(ParameterContext parameterContext) {
        return parameterContext.getParameter().getType().isAssignableFrom(List.class) ?
                extractListTypeParameter(parameterContext) :
                parameterContext.getParameter().getType();
    }

    private List<Object> scanImplementationsAndCreate(Class<?> interfaceToScan) {
        var implList = scan().stream()
                .filter(clazz -> !clazz.isInterface()
                        && clazz.isAnnotationPresent(Injectable.class)
                        && interfaceToScan.isAssignableFrom(clazz))
                .map(clazz -> {
                    try {
                        return createInstance(clazz);
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                })
                .toList();

        if (implList.isEmpty()) {
            throw new NoSuchElementException("no implementations found for " + interfaceToScan.getName());
        }

        return implList;
    }

    private Object scanImplementationAndCreate(Class<?> interfaceToScan) {
        var implList = scanImplementationsAndCreate(interfaceToScan);

        if (implList.size() > 1) {
            throw new NoSuchElementException("several implementations found for " + interfaceToScan.getName());
        }

        return implList.get(0);
    }

    @SneakyThrows
    private Object createInstance(Class<?> clazz) {
        var constructors = clazz.getConstructors();

        var noParamsConstructor = Arrays.stream(constructors)
                .filter(constructor -> constructor.getParameterCount() == 0).findFirst();

        if (noParamsConstructor.isPresent()) {
            return noParamsConstructor.get().newInstance();
        }

        // take first constructor and try to scan all implementations
        var constructor = constructors[0];

        var params = Arrays.stream(constructor.getParameterTypes()).map( clz -> {
            if (!clz.isInterface()) {
                throw new IllegalStateException("constructor parameters only interfaces supported for now!");
            }
            return scanImplementationAndCreate(clz);
        }).toArray();

        return constructor.newInstance(params);
    }

    Class<?> extractListTypeParameter(ParameterContext parameterContext) {
        var parameterType = parameterContext.getParameter().getParameterizedType();

        if (!(parameterType instanceof ParameterizedType
                && ((ParameterizedType) parameterType).getRawType().getTypeName().equals(List.class.getTypeName()))
        ) {
            return null;
        }

        var classes = Arrays.stream(((ParameterizedType) parameterType).getActualTypeArguments())
                .filter(arg -> arg instanceof Class && ((Class<?>) arg).isInterface())
                .map(arg -> (Class<?>) arg)
                .toArray(Class[]::new);

        return classes.length == 1 ? classes[0] : null;
    }

    @SneakyThrows
    private Set<Class<?>> scan() {
        if (loadedClasses != null) return loadedClasses;

        synchronized (lock) {
            if (loadedClasses != null) return loadedClasses;

            var cl = ClassLoader.getSystemClassLoader();

            loadedClasses = ClassPath.from(cl)
                    .getTopLevelClassesRecursive(this.getClass().getPackageName())
                    .stream()
                    .map(classInfo -> {
                        try {
                            return cl.loadClass(classInfo.getName());
                        } catch (ClassNotFoundException e) {
                            throw new RuntimeException(e);
                        }
                    }).collect(Collectors.toUnmodifiableSet());

            return loadedClasses;
        }
    }
}
