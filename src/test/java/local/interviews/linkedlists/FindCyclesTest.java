package local.interviews.linkedlists;

import local.interviews.ImplementationsScan;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.List;

@ExtendWith(ImplementationsScan.class)
public class FindCyclesTest {
    private final int[][] tests = new int[][]{{0, 0}, {0, 1}, {1, 0}, {1, 1}, {10, 0},
            {0, 4}, {0, 5}, {3, 7}, {7, 3}, {0, 20}, {10, 10}};

    @Test
    public void testCycle(List<FindCycle> solutions) {
        for (var solution : solutions) {
            var idx = 0;
            for (var test : tests) {
                generateAndRun(solution, test[0], test[1], idx);
                idx++;
            }
        }
    }

    @Test
    public void testCycleStart(List<FindCycleStart> solutions) {
        for (var solution : solutions) {
            var idx = 0;
            for (var test : tests) {
                generateAndRun(solution, test[0], test[1], idx);
                idx++;
            }
        }
    }

    private void generateAndRun(FindCycle solution, int beforeLoop, int loop, int run) {
        Assertions.assertEquals(loop > 0, solution.find(generate(beforeLoop, loop)),
                run + "_" + beforeLoop + "_" + loop);
    }

    private void generateAndRun(FindCycleStart solution, int beforeLoop, int loop, int run) {
        var node = solution.find(generate(beforeLoop, loop));
        var value = node != null ? node.value : null;

        Assertions.assertEquals(loop > 0 ? beforeLoop > 0 ? beforeLoop - 1 : 0 : null, value,
                run + "_" + beforeLoop + "_" + loop);
    }

    private LinkedListNode<?> generate(int beforeLoop, int loop) {
        LinkedListNode<Integer> first = null;
        LinkedListNode<Integer> loopStart = null;
        LinkedListNode<Integer> prev = null;
        LinkedListNode<Integer> current;

        for (int i = 0; i < beforeLoop + loop; i++) {
            current = new LinkedListNode<>(i, null);

            if (i == 0) first = current;
            if (prev != null) prev.next = current;

            if (loop > 0) {
                if (i == beforeLoop - 1 || (beforeLoop == 0 && loopStart == null)) {
                    loopStart = current;
                }

                if (i == beforeLoop + loop - 1) {
                    current = current.next = loopStart;
                }
            }

            prev = current;

        }

        return first;
    }
}
