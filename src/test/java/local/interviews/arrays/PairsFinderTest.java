package local.interviews.arrays;

import local.interviews.ImplementationsScan;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.Arrays;
import java.util.List;

@ExtendWith(ImplementationsScan.class)
public class PairsFinderTest {
    @Test
    public void testExactMatch(List<PairsFinderExact> solutions) {
        for (PairsFinderExact solution : solutions) {
            runSolution(solution, new int[]{0}, 6, new int[0], 1);
            runSolution(solution, new int[]{3, 4, -1, 0, 7}, 6, new int[]{-1, 7}, 2);
            runSolution(solution, new int[]{-100, -30, -5, 0, 10, 48, 200, 1000, 5000, 10000}, 995, new int[]{-5, 1000}, 3);
            runSolution(solution, new int[]{5, 2, 6, 10}, 4, new int[0], 4);
            runSolution(solution, new int[]{23, 1, 100, 67, 3, -200, 500}, 300, new int[]{-200, 500}, 5);
            runSolution(solution, new int[]{-2, -100, -60, -500}, -502, new int[]{-500, -2}, 6);
            runSolution(solution, new int[]{5, 2, 6, 11, 5}, 10, new int[]{5, 5}, 7);
            runSolution(solution, new int[]{5, 2, 6, 11, 20}, 31, new int[]{11, 20}, 8);
        }
    }

    @Test
    public void testNearestMatch(List<PairsFinderClosest> solutions) {
        for (PairsFinderClosest solution : solutions) {
            runSolution(solution, new int[]{0}, 6, new int[0], 1);
            runSolution(solution, new int[]{-1, 3, 4, 0, 7}, 5, new int[]{-1, 7}, 2);
            runSolution(solution, new int[]{-100, -30, -5, 0, 10, 48, 200, 1000, 5000, 1}, -3, new int[]{-5, 1}, 3);
            runSolution(solution, new int[]{5, 2, 6, 10}, 4, new int[]{2, 5}, 4);
            runSolution(solution, new int[]{5, 2, 6, 11, 5}, 10, new int[]{5, 5}, 5);
            runSolution(solution, new int[]{23, 1, 100, 67, 3, -200, 500}, 300, new int[]{-200, 500}, 6);
            runSolution(solution, new int[]{-2, -100, -60, -500}, -502, new int[]{-500, -2}, 7);
            runSolution(solution, new int[]{5, 2, 6, 11, 20}, 32, new int[]{11, 20}, 8);
        }
    }

    private void runSolution(PairsFinder solution, int[] n, int m, int[] ref, int index) {
        var result = solution.find(n, m);
        Arrays.sort(result);
        Assertions.assertArrayEquals(ref, result, solution.getClass().getName() + ":" + index);
    }
}
