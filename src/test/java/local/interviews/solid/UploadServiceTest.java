package local.interviews.solid;

import local.interviews.ImplementationsScan;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.Arrays;

@ExtendWith(ImplementationsScan.class)
public class UploadServiceTest {
    private static final Byte[] invalid = new Byte[200];
    private static final Byte[] jpeg2400b = new Byte[2400];
    private static final Byte[] jpeg4096b = new Byte[4096];
    private static final Byte[] jpeg1m = new Byte[1024 * 1024];
    private static final Byte[] jpeg1mPlus = new Byte[1024 * 1024 + 1];
    private static final Integer[] ref1mOutput = new Integer[1024];

    @BeforeAll
    public static void beforeAll() {
        Arrays.fill(jpeg2400b, (byte) 0x00);
        jpeg2400b[0] = (byte) 0xff;
        jpeg2400b[1] = (byte) 0xd8;

        Arrays.fill(jpeg4096b, (byte) 0x00);
        jpeg4096b[0] = (byte) 0xff;
        jpeg4096b[1] = (byte) 0xd8;

        Arrays.fill(invalid, (byte) 0x00);

        Arrays.fill(jpeg1m, (byte) 0x00);
        jpeg1m[0] = (byte) 0xff;
        jpeg1m[1] = (byte) 0xd8;

        Arrays.fill(jpeg1mPlus, (byte) 0x00);
        jpeg1mPlus[0] = (byte) 0xff;
        jpeg1mPlus[1] = (byte) 0xd8;

        Arrays.setAll(ref1mOutput, (i) -> 1024 * (i + 1));
    }

    @Test
    public void test1mbJpeg(UploadService uploadService) {
        Assertions.assertArrayEquals(ref1mOutput, uploadService.upload(Arrays.stream(jpeg1m)).toArray(Integer[]::new));
    }

    @Test
    public void test2400bJpeg(UploadService uploadService) {
        Assertions.assertArrayEquals(new Integer[]{1024, 2048, 2400}, uploadService.upload(Arrays.stream(jpeg2400b)).toArray(Integer[]::new));
    }

    @Test
    public void test4096bJpeg(UploadService uploadService) {
        Assertions.assertArrayEquals(new Integer[]{1024, 2048, 3072, 4096}, uploadService.upload(Arrays.stream(jpeg4096b)).toArray(Integer[]::new));
    }

    @Test
    public void testJpegInvalid(UploadService uploadService) {
        Assertions.assertThrows(IllegalArgumentException.class, () -> uploadService.upload(Arrays.stream(invalid)).count());
    }

    @Test
    public void test1mbPlusJpeg(UploadService uploadService) {
        Assertions.assertThrows(IllegalStateException.class, () -> uploadService.upload(Arrays.stream(jpeg1mPlus)).count());
    }
}